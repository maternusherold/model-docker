FROM tensorflow/tensorflow:1.14.0-gpu-py3-jupyter
WORKDIR /usr/src/model

RUN apt-get update && apt-get install -y --no-install-recommends \
	curl \
	vim \
	git && \
	rm -rf /var/lib/apt/lists/*

RUN curl -o ~/miniconda.sh -O  https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh  && \
     chmod +x ~/miniconda.sh && \
     ~/miniconda.sh -b -p /opt/conda && \
     rm ~/miniconda.sh
ENV PATH /opt/conda/bin:$PATH

COPY contents ./contents 
COPY environment.yml .
RUN conda env create -f environment.yml && conda init bash

