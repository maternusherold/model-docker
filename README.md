# Model Docker

Providing a Dockerfile for an easy deployment of the model training described in my thesis.

## Usage

### Running an instance

First, prepare a directory with the training images which is used in step 3.

1. clone the repository
2. build the image by running `docker build -t model-docker .`
3. execute a container by running `docker run -it -p 8000:8000 --runtime=nvidia --mount type=bind,src=<drr-dir>,target=/root/drr model-docker /bin/bash`. This will create an instance of the before created image and log in to the shell. Also, the host's port 8000 will be mapped to the docker's port 8000 and the training image directory will be mounted in the docker under `root/drr`.
4. activate the conda environment by running `conda activate carm`
5. export the pythonpath by running `export PYTHONPATH="$PWD/contents"`
6. start a jupyter notebook server by running `jupyter notebook --ip=0.0.0.0 --port=8000 --allow-root`
7. to display the notebook on the localhost on port 8000 forward the ports by running `ssh -L 8000:localhost:8000 -i ~/.ssh/<file> -l <user> <address>`. The notebook can then be accessed by in the browser on `localhost:8000`.

To save the model output a nother volume should be provided to the container instantiated in step 3.
