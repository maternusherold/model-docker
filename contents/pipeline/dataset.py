#!/usr/env/bin python

import numpy as np
import os
import pandas as pd
import pathlib
import tensorflow as tf

from pipeline.pipeline_utils import load_image_as_array, tensor_log_base, save_scaler
from sklearn.base import TransformerMixin
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from typing import List, Tuple
from utils import print_info


class DRRDataset:

    # TODO: add verbosity for generated data set
    #   total data size
    #   total training, validation and testing size

    def __init__(self, path_to_images: str, file_pattern: str,
                 path_to_label_table: str, feature_columns: List[str],
                 label_columns: List[str],
                 train_val_test_split: Tuple[float, float, float] = (.7, .15, .15),
                 batch_size: float = 32, scaler: TransformerMixin = MinMaxScaler(),
                 scaler_save_to: str = 'scaler.pickle'):
        """TODO: docstring on DRRDataset

        :param path_to_images:
        :param file_pattern:
        :param path_to_label_table:
        :param feature_columns:
        :param label_columns:
        :param train_val_test_split:
        :param batch_size:
        :param scaler:
        :param scaler_save_to:
        :var image_paths:
        :var dataset_size:
        :var dataset_training_size:
        :var dataset_validation_size:
        :var dataset_testing_size:
        :var datasets: tuple of training, validation and testing datasets
        """
        self.data_root = pathlib.Path(path_to_images)
        self.file_type = file_pattern
        self.path_to_label_table = path_to_label_table
        self.features = feature_columns
        self.labels = label_columns
        self.dataset_split = train_val_test_split
        # self.image_paths = self.get_image_paths()
        self.batch_size = batch_size
        self.scaler = scaler
        self.scaler_path = scaler_save_to
        self.dataset_size = self.load_data_table().shape[0]
        self.dataset_training_size = self.dataset_size * self.dataset_split[0]
        self.dataset_validation_size = self.dataset_size * self.dataset_split[1]
        self.dataset_testing_size = self.dataset_size * self.dataset_split[2]
        self.datasets = self.create_dataset()

    def load_data_table(self, separator: chr = ',') -> pd.DataFrame:
        """TODO: docstring

        :return: data table with file names as labels
        """
        return pd.read_csv(self.path_to_label_table, sep=separator)

    def _name_to_path(self, file_name) -> str:
        """TODO: docstring

        :param file_name:
        :return:
        """
        # FIXME: does not take ~/... as home into account
        path_to_file = os.path.abspath(
            os.path.join(self.data_root, ''.join([*file_name, self.file_type])))
        if not os.path.isfile(path_to_file):
            raise ValueError(f'No such file: {path_to_file}')
        return path_to_file

    def scale_labels(self, data_table, scaler) -> pd.DataFrame:
        """Computes standardized label columns.

        :param data_table:
        :param scaler: callable scaling method
        :return:
        """
        # scale columns
        data_table[self.labels] = scaler.fit_transform(data_table[self.labels])

        print_info(f'saving scaler to {os.path.abspath(self.scaler_path)}')
        save_scaler(scaler, self.scaler_path)

        print_info(f'column means:\n{np.mean(data_table)}'
                   + '\n' + f'column variances:\n{np.var(data_table)}')

        return data_table

    def file_name_to_file_path(self, data_table) -> pd.DataFrame:
        """TODO: docstring

        :return:
        """
        data_table[self.features] = data_table[self.features].apply(self._name_to_path,
                                                                    raw=True, axis=1)
        return data_table

    def dataset_from_data_frame(self, data_table: pd.DataFrame) -> tf.data.Dataset:
        """Creates a TensorFlow dataset from a pandas data frame.

        :param data_table:
        :return: tensorflow dataset representation of data frame
        """
        dataset = tf.data.Dataset.from_tensor_slices((
            data_table[self.features].to_numpy().flatten(order='C'),
            data_table[self.labels].to_numpy())
        )
        return dataset

    def create_dataset_from_table(self) -> Tuple[tf.data.Dataset,
                                                 tf.data.Dataset, tf.data.Dataset]:
        """TODO: docstring

        :return:
        """
        # data splits have to add up to one
        if np.sum(self.dataset_split) != 1:
            raise ValueError(f'splits do not add up to 1: '
                             f'{np.sum(self.dataset_split)} != 1')

        data_table = self.load_data_table()
        data_table = self.file_name_to_file_path(data_table=data_table)
        data_table = self.scale_labels(data_table=data_table, scaler=self.scaler)

        # split data table according to provided data splits
        try:
            not_training_split = self.dataset_split[1] + self.dataset_split[2]
            not_validation_split = self.dataset_split[2] / not_training_split
        except ZeroDivisionError:
            not_training_split = 0.0
            not_validation_split = 0.0
            print(f'division by zero encountered with splits: {self.dataset_split}')
            print(f'setting validation and test split to '
                  f'{not_training_split} and {not_validation_split}')

        train, test = train_test_split(data_table, test_size=not_training_split)
        validation, test = train_test_split(test, test_size=not_validation_split)

        return self.dataset_from_data_frame(train), \
               self.dataset_from_data_frame(validation), \
               self.dataset_from_data_frame(test)

    @staticmethod
    def preprocess_image(image: np.array) -> tf.Tensor:
        """TODO: docstring

        :param image:
        :return:
        """
        # FIXME: might also resize to network input
        image = tf.image.resize(image, [224, 224])
        image = image / 255.
        # image = tensor_log_base(image, base=10)
        return image

    def load_and_preprocess(self, path, labels) -> Tuple[tf.Tensor, list]:
        """TODO: docstring

        :param path:
        :param labels:
        :return:
        """
        # TODO: does not provide option for resizing
        img = load_image_as_array(path)
        return self.preprocess_image(img), labels

    def preprocess_dataset(self, dataset: tf.data.Dataset) -> tf.data.Dataset:
        """Prepares dataset according to pipeline configuration.

        Applying mappings, shuffle etc. as well a batching and prefetching.

        :param dataset: dataset to prepare
        :return: final dataset
        """
        # make system depending decisions
        AUTOTUNE = tf.data.experimental.AUTOTUNE

        # large buffer_size might account for lagging -> decrease in case
        dataset = dataset.shuffle(buffer_size=self.dataset_size)
        dataset = dataset.repeat(count=None)

        dataset = dataset.map(map_func=self.load_and_preprocess,
                              num_parallel_calls=AUTOTUNE)
        dataset = dataset.batch(batch_size=self.batch_size)

        dataset = dataset.apply(
            tf.data.experimental.prefetch_to_device('/device:GPU:0',
                                                    buffer_size=AUTOTUNE))
        return dataset

    def create_dataset(self, prefetch_buffer: float = 4, verbose: bool = True) \
            -> Tuple[tf.data.Dataset, tf.data.Dataset, tf.data.Dataset]:
        """TODO: docstring

        TODO: better flexibility for buffer_size and num_parallel_calls etc.

        :return:
        """
        with tf.device('/cpu:0'):
            # create base datasets of image paths and labels
            train, val, test = self.create_dataset_from_table()
            dataset_training = self.preprocess_dataset(train)
            dataset_validation = self.preprocess_dataset(val)
            # TODO: test set does not have to be on GPU -> no prefetching
            dataset_testing = self.preprocess_dataset(test)

        if verbose:
            print_info(f'total dataset size: {self.dataset_size}' + '\n'
                       + f'steps per epoch: '
                         f'{np.ceil(self.dataset_training_size / self.batch_size)} '
                         f'and training size: {self.dataset_training_size}' + '\n'
                       + f'steps per epoch: '
                         f'{np.ceil(self.dataset_validation_size / self.batch_size)} '
                         f'and validation size: {self.dataset_validation_size}')

        return dataset_training, dataset_validation, dataset_testing
