#!/usr/env/bin python

import numpy as np
import os
import pickle
import sys
import tensorflow as tf

from sklearn.base import TransformerMixin


def load_image_as_array(path: str) -> np.array:
    """TODO: docstring

    :param path:
    :return:
    """
    image = tf.read_file(path)
    image = tf.image.decode_png(image, channels=3)
    return image


def tensor_log_base(array, base: int = 10):
    """Calculates the logarithm to a arbitrary base a Tensorflow does not
    support this feature natively.

    :param array: array to take the logarithm
    :param base: base to logarithm
    :return:
    """
    numerator = tf.log(array + 1)
    denominator = tf.log(tf.constant(base, dtype=numerator.dtype))
    return numerator / denominator


def save_scaler(scaler: TransformerMixin, save_to: str):
    """Saving a TransformerMixin object to dedicated path.

    :param scaler: scaler object to save to
    :param save_to: path to save to incl. file name
    """
    try:
        with open(save_to, 'wb') as file:
            pickle.dump(scaler, file=file)
    except:
        print("Unexpected error:", sys.exc_info()[0])


def load_scaler(path_to_scaler: str) -> TransformerMixin:
    """Loading TransformerMixin object from pickle.

    :param path_to_scaler:
    :return: scaler like MinMaxScaler
    """
    try:
        with open(path_to_scaler, 'rb') as file:
            return pickle.load(file)
    except:
        print("Unexpected error:", sys.exc_info()[0])
