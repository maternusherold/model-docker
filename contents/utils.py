#!/usr/env/bin python

"""
Utility function necessary for the complete project.
"""


def print_info(info: str):
    """Prints provided info in a special format.

    :param info: information to print as string
    """
    print()
    print(f'[INFO] '.ljust(80, '='))
    print(info)
    print(''.ljust(80, '='))
