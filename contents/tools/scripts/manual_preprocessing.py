#!/usr/env/bin python

"""
To inspect the pipeline's preprocessing step by step and fine grained inspection
of a model's predictions the preprocessing is rebuilt and can be applied outside
of the data pipeline itself.

For human 'inspectability' the log10 transform should be omitted to have
greater ranges in pixel values and not moving values even further to 0 (black).
"""

import numpy as np

from PIL import Image
from typing import Tuple


def load_img(path_to_image: str) -> np.array:
    """Loads image as numpy array.

    :param path_to_image: path to image
    :return: image as numpy array representation
    """
    image = Image.open(path_to_image)
    return np.array(image)


def apply_resizing(image: np.array, resize_to: Tuple[int, int]) -> np.array:
    """Resizing image to goal dimension.

    :param image: image as numpy array
    :param resize_to: tuple defining target dimensions
    :return: resized image as numpy array
    """
    image = Image.fromarray(image)
    image = image.resize(resize_to, Image.NEAREST)
    return np.array(image)


def apply_normalization_image(image: np.array) -> np.array:
    """Normalizes a image to [0,1].

    :param image: image as numpy array
    :return: normalized image as numpy array
    """
    return image / 255.


def apply_log_transform(image: np.array) -> np.array:
    """Computes log to base 10 of image.

    :param image: image as numpy array
    :return: log-transformed image as numpy array
    """
    return np.log10(image + 1)


def preprocess_image(path_to_image: str, resize_to: Tuple[int, int] = (224, 224),
                     human_visibility: bool = False, verbose: bool = True) -> np.array:
    """Loads and preprocesses image.

    Transformations applied: resizing, normalization, log10

    :param path_to_image: path to image
    :param resize_to: tuple defining target dimensions
    :param human_visibility: to omit log transform for greater range
    :param verbose: whether to print image status
    :return: loaded and processed image as numpy array
    """
    image = load_img(path_to_image=path_to_image)
    if verbose:
        print(f'pure image - max/min: {image.max(), image.min()}')
    image = apply_resizing(image=image, resize_to=resize_to)
    if verbose:
        print(f'resized image - max/min: {image.max(), image.min()}')
    image = apply_normalization_image(image=image)
    if verbose:
        print(f'normalized image - max/min: {image.max(), image.min()}')

    if not human_visibility:
        image = apply_log_transform(image=image)
        if verbose:
            print(f'log image - max/min: {image.max(), image.min()}')

    return np.array(image, dtype=np.float32)


if __name__ == '__main__':
    # path_to_image = '/home/maternus/Coding/thesis_ubc/carm-position-prediction/' \
    #                 'data/converted_data/complex_00120_90850_0_.png'
    path_to_image = '/home/maternus/Coding/thesis_ubc/carm-position-prediction/' \
                    'data/converted_data/complex_-402020_120550_0_.png'
    image = preprocess_image(path_to_image=path_to_image, human_visibility=True)

    import matplotlib.pyplot as plt
    plt.imshow(image)
    plt.show()
