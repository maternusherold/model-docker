#!/usr/env/bin python

import argparse
import numpy as np
import os

from PIL import Image
from typing import List


def load_tiff_to_array(file_path: str) -> np.array:
    """Returns tff image as numpy array.

    :param file_path: path to file
    :return: numpy array representation of tiff image
    """
    image = Image.open(file_path)
    return np.array(image)


def scale_to_range(value, min_current, max_current,
                   min_goal: int = 0, max_goal: int = 255):
    """Computes the relative scaling from a current scale to a new scale.

    :param value: value or numpy array to rescale
    :param min_current: current ranges min
    :param max_current: current ranges max
    :param min_goal: anticipated scale's min
    :param max_goal: anticipated scale's max
    :return: rescaled value or numpy array
    """
    scaled = ((value - min_current) * (max_goal - min_goal)) / \
             (max_current - min_current) + min_goal
    return scaled


def convert_tiff_to_png(file_path: str, destination_path: str):
    """Converts tiff file to png

    :param file_path: path to tiff image
    :param destination_path: path to destination folder
    """
    new_file_path = file_path.split('.')[0]
    new_file_path = new_file_path.split('/')[-1]
    image = load_tiff_to_array(file_path)
    # convert to log10 scale
    image = np.log10(image + 1)

    # standardize pixel values to RGB scale
    image = scale_to_range(image, min_current=image.min(), max_current=image.max(),
                           min_goal=0, max_goal=255)
    # stack imag to be 3D
    if len(image.shape) == 2:
        image = np.stack((image, image, image), axis=2)
    # save image as png
    save_to = os.path.join(destination_path, new_file_path + '.png')
    Image.fromarray(image.astype(np.uint8)).save(save_to, 'PNG', quality=100)


def convert_tiffs_to_png(tiff_paths: List[str], destination_path: str):
    """Converts all tiffs to png

    :param tiff_paths:
    :param destination_path:
    """
    for tiff in tiff_paths:
        convert_tiff_to_png(tiff, destination_path=destination_path)


def convert_tiff_dir(path_to_dir: str, destination_path: str, verbose: bool = False):
    """Converts all tiffs in given directory.

    :param path_to_dir: path to directory
    :param destination_path:
    :param verbose:
    """
    for file in os.listdir(path_to_dir):
        if file.split('.')[-1].lower() == 'tiff':
            if verbose:
                print(f'working on {os.path.join(file)}')
            convert_tiff_to_png(os.path.join(
                os.path.abspath(path_to_dir), file), destination_path=destination_path)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument('-p', '--path', help='path to directory',
                        required=True)
    parser.add_argument('-d', '--dpath', help='path to dest directory',
                        required=True)
    args = parser.parse_args()

    convert_tiff_dir(args.path, args.dpath)
