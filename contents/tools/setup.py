from setuptools import setup

with open("README.md", "r") as f:
    long_description = f.read()

setup(name='DeepDRR',
      version='0.1.0',
      description='DRR generating Framework using machine learning for '
                  'segmentation and augmentation',
      long_description=long_description,
      long_description_content_type='text/markdown',
      url='https://gitlab.com/maternusherold/carm-position-prediction',
      author='Maternus Herold',
      author_email='turnmanh@gmail.com',
      license='GNU',
      packages=['deep_drr'],
      install_requires=[
          'numpy',
          'pandas',
          'pycuda',
          'pydicom',
          'scikit-image',
          'tensorboard',
          'tensorboardX',
          'torch',
          'torchvision'
      ],
      scripts=['scripts/create_vm.sh'],
      entry_points={
          'console_scripts': ['deep-drr=deep_drr.interface:main']
      },
      classifiers=[
          'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
          'Programming Language :: Python :: 3.7',
          'Operating System :: OS Independent'
      ],
      zip_safe=False)
