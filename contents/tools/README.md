# Modularized DeepDRR
as the module from [GitHub](https://github.com/mathiasunberath/DeepDRR) is not 
yet very flexible nor provides a standard interface or amenities to install all 
dependencies at once etc. I decided to modularize DeepDRR for my use case.


## Documentation
The DeepDRR framework produces a digitally reconstructed radiography (DRR) based 
on a provided CT volume. Through setting a series of parameters these can be 
calibrated and configured, i.e. via setting the angles and camera attributes. 
An illustration of the setup can be found [here](http://dicom.nema.org/medical/dicom/current/output/chtml/part03/figures/PS3.3_C.8.19.6-2.svg).
 

#### Main settings
* rotation angles: rotating the object in its 3D space to simulate shots from 
different perspectives  
    * `theta`: rotation around right-left-axis &rarr; pitch
    * `phi`: rotating around inferior-superior-axis &rarr; roll
    * `rho`: rotating around posterior-anterior-axis &rarr; yaw 
* `origin` as center of CT volume relative to the projector moves the object rel. 
to the projector in the x,y,z system with `[RL-axis, AP-axis, IS-axis]`
    * e.g. given a CT volume of a object in HFS (head first - supine) orientation 
    a `origin=[0, 0, 120]` would move the imaged section towards the lower part 
    of the object where as `origin=[0, 0, -120]` would move towards the upper part.
    An illustration can be found [here](http://dicom.nema.org/dicom/2013/output/chtml/part17/figures/PS3.17_FFF.1.2-5.svg)
     

#### Main advantages over conventional DRRs
* **TODO**


## Changes to org. code base
* modularize to package `deep_drr`
* create environment.yml and install pytorch with CUDA support
* minor changes in paths to use pythons `os.path` instead of hard coding; 
    e.g. when building paths 
* utility function in `utils.py`
* data generator in `data_generator.py`
* CT volume object in `data_generator.py`
* **docstrings and documentation**!


## Urgent TODOs
* refactoring to meet/get close to PEP8
* check for path incompatibilities; change to os-independent version
* implement logging  
* documentation
