#!/usr/bin/env python

import itertools
import numpy as np
import os
import pandas as pd
from tools.deep_drr.add_scatter import ScatterNet
from tools.deep_drr.analytic_generators import add_noise
from tools.deep_drr.mass_attenuation_gpu import calculate_intensity_from_spectrum as \
    spectrum_intensity
from tools.deep_drr.utils import image_saver
from tools.deep_drr.projector import generate_projections
from tools.deep_drr.utils import Camera
from tools.deep_drr import load_dicom
from tools.deep_drr.projection_matrix import generate_uniform_angels
from tools.deep_drr.projection_matrix import generate_projection_matrices_from_values
from tools.deep_drr.spectrum_generator import SPECTRUM120KV_AL43
from typing import Tuple
from utils import print_info


class DataGenerator:

    # TODO: getter and setter for all attributes
    # TODO: shorten to sample size

    def __init__(self, complete_segmentation: bool = True, angle_step: int = 5,
                 translation_step: int = 5, phi_range: Tuple[int, int] = (0, 180),
                 theta_range: Tuple[int, int] = (0, 180),
                 rho_range: Tuple[int, int] = (0, 0),
                 origin_x_range: Tuple[int, int] = (0, 0),
                 origin_y_range: Tuple[int, int] = (0, 0),
                 origin_z_range: Tuple[int, int] = (0, 0),
                 cartesian_prod: bool = True, camera: Camera = None,
                 ct_volume_path: str = None, save_to_dir: str = './generated_data',
                 random_pick: bool = True, sample_size: int = None,
                 spectrum=SPECTRUM120KV_AL43, scatter: bool = False,
                 photon_count: int = 100000):
        """DataGenerator facilitates arbitrary long DRR creation with DeepDRR.

        :param complete_segmentation: whether to include soft tissue in segmentation
        :param angle_step: step size sampling angles for phi, theta and rho
        :param translation_step: step size sampling shifts in x,y,z direction
        :param phi_range: range of possible phi angles - roll
        :param theta_range: range of possible phi angles - pitch
        :param rho_range: range of possible phi angles - yaw
        :param origin_x_range: range of possible shifts in x direction
        :param origin_y_range: range of possible shifts in z direction
        :param origin_z_range: range of possible shifts in z direction
        :param cartesian_prod: creates cartesian product of all possible values
        :param camera: x-ray machine
        :param ct_volume_path: path to ct slices
        :param save_to_dir: directory to save DRRs to
        :param random_pick: randomly pick param settings to meet sample_size
        :param sample_size: limits data set size in case the size is reached
        :param spectrum: spectrum mimicking x-ray spectrum
        :param scatter: whether to augment scatter
        :param photon_count
        :var data_table: array holding parameters and name per DRR
        :var ct_volume: CTVolume object
        :var materials: dictionary of material masks
        """
        self.complete_segmentation = complete_segmentation
        self.angle_step = angle_step
        self.translation_step = translation_step
        self.phi_range = phi_range
        self.theta_range = theta_range
        self.rho_range = rho_range
        self.origin_x_range = origin_x_range
        self.origin_y_range = origin_y_range
        self.origin_z_range = origin_z_range
        self.cartesian_prod = cartesian_prod
        self.camera = camera
        self.ct_path = ct_volume_path
        self.output_dir = save_to_dir
        self.random_pick = random_pick
        self.sample_size = sample_size
        self.phis = self.compute_range(phi_range, angle_step)
        self.thetas = self.compute_range(theta_range, angle_step)
        self.rhos = self.compute_range(rho_range, angle_step)
        self.origin_x = self.compute_range(origin_x_range, translation_step)
        self.origin_y = self.compute_range(origin_y_range, translation_step)
        self.origin_z = self.compute_range(origin_z_range, translation_step)
        self.spectrum = spectrum
        self.scatter = scatter
        self.photon_count = photon_count
        self.data_table = None
        self.ct_volume = None
        self.materials = None
        self.create_data_table()

    @staticmethod
    def compute_range(data_range: Tuple[float], step_size: int) -> np.array:
        """Computes values in provided range.

        :param data_range: 2D tuple specifying data boundaries
        :param step_size: step size to sample values
        :return: array of values in range
        """
        return np.arange(data_range[0], data_range[1] + 1, step_size)

    @staticmethod
    def create_string(*args) -> str:
        """Creates string from provided arguments

        :param args: objects to cast and append to string
        :return: string of provided objects
        """
        return ''.join(['%s' % item for item in args])

    @staticmethod
    def create_file_names(parameters: pd.DataFrame,
                          create_name_func: callable) -> pd.DataFrame:
        """Creates file name per row data according to naming function.

        :param parameters: X,6 dimensional DataFrame of parameter settings
        :param create_name_func: function creating desired names per row
        :return: DataFrame of file names
        """
        # TODO: more elegant way to reference columns
        return np.vectorize(create_name_func)(parameters['theta'], parameters['phi'],
                                              parameters['rho'], parameters['origin_x'],
                                              parameters['origin_y'],
                                              parameters['origin_z'])

    def load_ct_volume(self, **kwargs):
        """Loads ct volume from path.

        To define the loaded data more detailed keyword arguments can be passed
        which are then forwarded to the CTVolume constructor.

        e.g. to set a different SliceThickness when working with DICOM images
        one would pass `fall_back_slice_thickness=2.0` as this is the CTVolume
        attribute for slice thickness.

        :param kwargs: keyword parameters forwarded to CTVolume object; have to
                match CTVolume attributes
        """
        self.ct_volume = CTVolume(self.ct_path, **kwargs)
        self.materials = self.ct_volume.segmentation
        if not self.complete_segmentation:
            # materials with out bone segmentation
            self.materials = self.materials.pop('bone')

    def create_data_table(self):
        """Creates and fills data_table based on settings. """
        # TODO: sanity check that data_table has correct dimensions
        parameters = self.create_parameter_combinations()
        parameters['file_names'] = self.create_file_names(parameters,
                                                          self.create_file_name)
        print_info(f'Dataset size: {parameters.shape}')
        self.data_table = parameters

    def create_parameter_combinations(self):
        """Creates data frame holding all possible param settings.

        :return: data frame parameter settings
        """
        # number_origins = len(self.origin_x) * len(self.origin_y) * len(self.origin_z)
        # number_angles = len(self.phis) * len(self.thetas) * len(self.rhos)

        # creates data frame of all possible parameter combinations
        df = pd.DataFrame(data=itertools.product(self.thetas, self.phis, self.rhos,
                                                 self.origin_x, self.origin_y,
                                                 self.origin_z),
                          columns=['theta', 'phi', 'rho', 'origin_x',
                                   'origin_y', 'origin_z'])
        return df

    def create_file_name(self, angle_theta: int, angle_phi: int, angle_rho: int,
                         origin_x: int, origin_y: int, origin_z: int) -> str:
        """Creates file name for parameter setting.

        A file name will be created based on the parameter settings and following
        the pattern <simple/complex> + _ + <origin> + _ + <angles>

        :param angle_theta: current theta angle
        :param angle_phi: current phi angle
        :param angle_rho: current rho angle
        :param origin_x: x coordinate of current origin
        :param origin_y: y coordinate of current origin
        :param origin_z: z coordinate of current origin
        :return: file name
        """
        prefix = 'complex' if self.complete_segmentation else 'simple'
        file_name = prefix + '_' + self.create_string(origin_x, origin_y, origin_z) \
                    + '_' + self.create_string(angle_theta, angle_phi, angle_rho)
        return file_name

    def create_images_from_settings(self, drr_function: callable = None):
        """Creates one image per setting and saves to the dedicated directory.

        Based on the created data_table a DRR will be created per parameters
        setting by iterating over the table rows.
        The DRRs and parameter settings will be saved to the specified
        output_dir - the DRRs as .tiff, the data_table as .csv
        """
        # set default function to create_deep_drr
        if drr_function is None:
            drr_function = self.create_deep_drr

        # check if dest directory exists
        if not os.path.exists(self.output_dir):
            os.mkdir(self.output_dir)

        # save data table
        self.data_table.to_csv(os.path.join(self.output_dir, 'parameter_settings.csv'),
                               header=True, index=False)

        # load CT volume if not done in prior
        if self.ct_volume is None:
            self.load_ct_volume()

        # create image per parameter set
        self.data_table.apply(drr_function, axis=1)

    def create_deep_drr(self, parameter_settings: pd.Series):
        """Creates DRR based on provided parameter settings.

        :param parameter_settings: list or parameters defining the image as
                defined in create_data_table
        """
        # create list of origin coordinates to provide to DeepDRR
        origin_shift = [parameter_settings['origin_x'],
                        parameter_settings['origin_y'],
                        parameter_settings['origin_z']]

        # calculate angles in radians
        # TODO: change to slimmer method call
        thetas, phis = generate_uniform_angels(parameter_settings['theta'],
                                               parameter_settings['theta'],
                                               parameter_settings['phi'],
                                               parameter_settings['phi'],
                                               self.angle_step, self.angle_step)

        # calculate projections matrices per setting
        # TODO: provide offset option as well
        projection_matrices = generate_projection_matrices_from_values(
            source_to_detector_distance=self.camera.source_to_detector_distance,
            pixel_dim_x=self.camera.pixel_size, pixel_dim_y=self.camera.pixel_size,
            sensor_size_x=self.camera.sensor_width,
            sensor_size_y=self.camera.sensor_height,
            isocenter_distance=self.camera.isocenter_distance, phi_list=phis,
            theta_list=thetas)

        # apply forward projection on volume per material
        fwd_projections = generate_projections(
            projection_matrices=projection_matrices, density=self.ct_volume.volume,
            materials=self.materials, origin=origin_shift,
            voxel_size=self.ct_volume.voxel_size, sensor_width=self.camera.sensor_width,
            sensor_height=self.camera.sensor_height, mode="linear",
            max_blockind=200, threads=8
        )

        # calculate intensity at detector
        image, photon_probability = spectrum_intensity(fwd_projections,
                                                       self.spectrum)

        # apply further augmentation 
        # TODO: weight error in add_noise
        #   RuntimeError: Input type (torch.cuda.DoubleTensor) and weight type
        #   (torch.cuda.FloatTensor) should be the same
        if self.scatter:
            image, photon_probability = self.augment_scatter(image, photon_probability)

        # transform to collected energy in keV per cm^2
        image = image * (self.photon_count / (self.camera.pixel_size *
                                              self.camera.pixel_size))

        # add poisson noise
        drr = add_noise(image, photon_probability, self.photon_count)

        # save final drr
        image_saver(drr, parameter_settings['file_names'], self.output_dir)
        # return drr

    def augment_scatter(self, image, photon_probability):
        """Augments image with scatter caused by diverting photons.

        :param image: DRR to add scatter to
        :param photon_probability: diverting probability
        :return: augmented image
        """
        # scatter_net = ScatterNet()
        scatter = ScatterNet().add_scatter(input_image=image, camera=self.camera)
        photon_probability *= 1 + scatter / image
        image += scatter
        return image, photon_probability


class CTVolume:

    def __init__(self, volume_path: str, file_extension: str = '.dcm',
                 smooth_air: bool = False, truncate: callable = None,
                 fall_back_slice_thickness: float = 1.0,
                 threshold_segmentation: bool = True):
        """Facilitates an CT volume and stores the loaded volume's data.

        TODO: support new_resolution in load_dicom as well
        :param volume_path: path to CT volume slices
        :param file_extension: file extension for volume files
        :param smooth_air: whether to smooth air in segmentation as well
        :param truncate: truncation to apply to volume
        :param fall_back_slice_thickness: value for SliceThickness if not present in files
        :param threshold_segmentation: whether to use threshold or VNet segmentation
        :var volume: matrix of volumes data points
        :var segmentation: bool matrix of segmentation
        :var voxel_size: voxel size for specific volume
        """
        self.volume_path = volume_path
        self.file_extension = file_extension
        self.smooth_air = smooth_air
        self.truncate = truncate
        self.fall_back_slice_thickness = fall_back_slice_thickness
        self.threshold_segmentation = threshold_segmentation
        self.volume = None
        self.segmentation = None
        self.voxel_size = None
        self.load_volume()

    def load_volume(self):
        """Utilizes load_dicom to load volume.

        TODO: sanity check returned values
        """
        volume, segmentation, voxel_size = load_dicom.load_dicom(
            source_path=self.volume_path, file_extension=self.file_extension,
            fixed_slice_thickness=self.fall_back_slice_thickness,
            smooth_air=self.smooth_air, truncate=self.truncate,
            use_thresholding_segmentation=self.threshold_segmentation)
        self.volume = volume
        self.segmentation = segmentation
        self.voxel_size = voxel_size


# TODO: remove for production
if __name__ == '__main__':
    volume_path = '/home/maternusherold/carm/data/ct_lymph/ct_lymph_003'
    output_path = '/home/maternusherold/carm/data/generated_data_AP_translation'
    obj = DataGenerator(theta_range=(90, 90), phi_range=(90, 90),
                        origin_x_range=(-80, 80), origin_y_range=(-100, 100),
                        origin_z_range=(-60, 120), translation_step=5,
                        angle_step=10, ct_volume_path=volume_path,
                        save_to_dir=output_path)
    obj.load_ct_volume(fall_back_slice_thickness=1.0)

    # 2x2 binning
    camera_22 = Camera(sensor_width=1240, sensor_height=960, pixel_size=0.31,
                       source_to_detector_distance=1000, isocenter_distance=750)

    # 4x4 binning
    camera_44 = Camera(sensor_width=620, sensor_height=480, pixel_size=0.62,
                       source_to_detector_distance=1000, isocenter_distance=750)

    obj.camera = camera_44
    obj.create_images_from_settings()
