import matplotlib.pyplot as plt
import numpy as np
import os
import deep_drr.mass_attenuation_gpu as mass_attenuation

from deep_drr import projector
from deep_drr import projection_matrix
from deep_drr.utils import image_saver, Camera, param_saver
from deep_drr.load_dicom import load_dicom
from deep_drr.analytic_generators import add_noise
from deep_drr import spectrum_generator
from deep_drr import add_scatter


def generate_projections_on_sphere(volume_path, save_path, min_theta: int, max_theta: int,
                                   min_phi: int, max_phi: int, spacing_theta: int,
                                   spacing_phi: int,
                                   photon_count, camera, spectrum,
                                   scatter=False, origin=[0, 0, 0]):
    # Use this if you have a volume
    # load and segment volume
    #   fixed slice thickness might not be the correct one but is needed as there
    #   also is no SliceLocation in the current volume.
    #   threshold segmentation might not be as good as V-net
    volume, materials, voxel_size = load_dicom(volume_path,
                                               fixed_slice_thickness=1.0,
                                               use_thresholding_segmentation=True)
    # generate angle pairs on a sphere
    thetas, phis = projection_matrix.generate_uniform_angels(min_theta, max_theta,
                                                             min_phi, max_phi,
                                                             spacing_theta, spacing_phi)

    # generate projection matrices - one per angle (theta, phi) pair
    proj_mats = projection_matrix.generate_projection_matrices_from_values(
        camera.source_to_detector_distance, camera.pixel_size, camera.pixel_size,
        camera.sensor_width, camera.sensor_height, camera.isocenter_distance,
        phis, thetas)


    # forward project densities
    forward_projections = projector.generate_projections(
        proj_mats, volume, materials, origin, voxel_size, camera.sensor_width,
        camera.sensor_height, mode="linear", max_blockind=200, threads=8)

    # calculate intensity at detector (images: mean energy one photon emitted
    #   from the source deposits at the detector element, photon_prob:
    #   probability of a photon emitted from the source to arrive at the detector)
    images, photon_prob = mass_attenuation.calculate_intensity_from_spectrum(
        forward_projections, spectrum)
    # add scatter
    if scatter:
        scatter_net = add_scatter.ScatterNet()
        scatter = scatter_net.add_scatter(images, camera)
        photon_prob *= 1 + scatter / images
        images += scatter

    # transform to collected energy in keV per cm^2
    images = images * (photon_count / (camera.pixel_size * camera.pixel_size))

    # add poisson noise
    images = add_noise(images, photon_prob, photon_count)

    # save images
    image_saver(images, "DRR", save_path)

    # save parameters
    param_saver(thetas=thetas, phis=phis, origin=origin, prefix='simulation_data',
                save_path=save_path, proj_mats=proj_mats, camera=camera,
                photons=photon_count, spectrum=spectrum, narrow_scope=False)

    # show result
    plt.imshow(np.log10(images[0, :, :]), cmap="gray")
    plt.savefig('test_image.png', dpi=300, )
    plt.show()


def main():
    # 2x2 binning
    # camera = Camera(sensor_width=1240, sensor_height=960, pixel_size=0.31,
    #                 source_to_detector_distance=1000, isocenter_distance=750)
    # 4x4 binning
    camera = Camera(sensor_width=620, sensor_height=480, pixel_size=0.62,
                    source_to_detector_distance=1000, isocenter_distance=750)

    # TODO: remove afterwards
    print(f'currently working in {os.getcwd()}')

    # define the path to your dicoms here or use the simple phantom from the code above
    dicompath = '../../data/ct_lymph/ct_lymph_003'

    save_path = './data/generated_data/test'
    min_theta = 81
    max_theta = min_theta  # 120
    min_phi = 95
    max_phi = min_phi
    spacing_theta = 30
    spacing_phi = 15
    photon_count = 10**6
    # origin [0, 0, 0] corresponds to the center of the volume and controls the
    #   shift or translation of the volume w.r.t. to the projector
    origin = [71, -88, 70]  # prop wrong see slicer
    spectrum = spectrum_generator.SPECTRUM120KV_AL43

    if not os.path.isdir(save_path):
        os.makedirs(save_path)

    generate_projections_on_sphere(volume_path=dicompath, save_path=save_path,
                                   min_theta=min_theta, max_theta=max_theta,
                                   min_phi=min_phi, max_phi=max_phi,
                                   spacing_theta=spacing_theta, spacing_phi=spacing_phi,
                                   photon_count=photon_count, camera=camera, spectrum=spectrum,
                                   origin=origin, scatter=False)


if __name__ == "__main__":
    main()
