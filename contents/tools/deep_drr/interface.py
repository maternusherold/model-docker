#!/usr/bin/env python

"""Providing a minimal command line interface for the DeepDRR framework.

The DeepDRR framework provides capabilities to generate digital reconstructed
radiographs based on a CT volume. Those might be used as training data and
DeepDRR therefore as synthetic data generator for machine learning projects.
This command line interface shell ease the process of generating those data sets.

IMPORTANT: the framework was initially developed by Mathias Unberath et al.

TODO: create CLI to generate X number of images of CT volume V
"""


def main():
    # TODO: provide CLI
    pass
