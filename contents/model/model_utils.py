#!/usr/env/bin python

import matplotlib.pyplot as plt
import numpy as np

from typing import Dict


def plot_metrics(metrics: Dict[str: np.array], **kwargs) -> plt:
    """TODO: docstring

    :param metrics:
    :return:
    """
    if kwargs['figsize']:
        plt.figure(figsize=kwargs['figsize'])
    else:
        plt.figure(figsize=(8, 8))

    for label, metric in metrics.items():
        plt.plot(metric, label=label)

    plt.legend(loc='lower right')
    plt.ylabel('Score')
    plt.ylim([min(plt.ylim()),1])
    plt.title(' '.join(['Score of ', list(metrics.keys())]))
    return plt
