#!/usr/env/bin python

import argparse
import numpy as np
import os
import tensorflow as tf
tf.enable_eager_execution()

from model.model import PositionPrediction
from pipeline.dataset import DRRDataset
from .train_utils import plot_metric

if __name__ == '__main__':
    # # CLI for ease of use
    # parser = argparse.ArgumentParser()
    # parser.add_argument('-m', '--model', help='pretrained model')
    # parser.add_argument('-b', '--batch-size', help='batch size for training',
    #                     default=32)
    # parser.add_argument('-e', '--epochs', help='epochs to train for',
    #                     default=10)
    # parser.add_argument('-p', '--path-to-files', help='path to files',
    #                     default='/home/maternusherold/carm/data/converted_data')
    # parser.add_argument('-t', '--path-to-table', help='path to data table',
    #                     default='/home/maternusherold/carm/data/'
    #                             'converted_data/parameter_settings.csv')
    # parser.add_argument('-f', '--features', help='features column name',
    #                     default="'file_names'")
    # parser.add_argument('-l', '--labels', help='labels column name',
    #                     default="'theta', 'phi', 'rho', 'origin_x', "
    #                             "'origin_y', 'origin_z'")
    # args = parser.parse_args()
    # print(args['--path-to-files'])

    ############################################################################
    # training parameters ######################################################
    BATCH_SIZE = 512
    EPOCHS = 10
    BASE_MODEL = 'MobileNetV2'
    ############################################################################
    # with tf.Session() as sess:
    #     init = tf.global_variables_initializer()
    #     sess.run(init)
    if True:
        tf.logging.set_verbosity(tf.logging.ERROR)
        # with tf.Session() as sess:
        # remote paths
        # path_to_files = '/home/maternusherold/carm/data/converted_data'
        # path_to_table = '/home/maternusherold/carm/data/converted_data/' \
        #                 'parameter_settings.csv'

        # local paths
        path_to_files = '/home/maternus/Coding/thesis_ubc/' \
                        'carm-position-prediction/data/generated_data'
        path_to_table = os.path.join(path_to_files, 'parameter_settings_steep_angles_pelvic_area.csv')

        feat_cols = ['file_names']
        label_cols = ['theta', 'phi', 'rho', 'origin_x', 'origin_y', 'origin_z']

        # prepare datasets
        drr_dataset = DRRDataset(path_to_images=path_to_files, file_pattern='_0_.tiff',
                                 path_to_label_table=path_to_table,
                                 feature_columns=feat_cols,
                                 label_columns=label_cols,
                                 batch_size=BATCH_SIZE)

        print(f'dataset output shapes: {tf.data.get_output_shapes(drr_dataset.datasets[0])}')
        print(f'dataset sizes - train/val: '
              f'{drr_dataset.dataset_training_size, drr_dataset.dataset_validation_size}')

        # retrieve datasets
        training_data, validation_data, test_data = drr_dataset.datasets

        steps_per_epoch = int(np.ceil(drr_dataset.dataset_training_size / drr_dataset.batch_size))
        validation_steps = int(np.ceil(drr_dataset.dataset_validation_size / drr_dataset.batch_size))

        print(f'\n[INFO] {"=" * 73}')
        print(f'dataset size: {drr_dataset.dataset_size}')
        print(f'steps per epoch: {steps_per_epoch} '
              f'and training size: {drr_dataset.dataset_training_size}')
        print(f'steps per epoch: {validation_steps} and '
              f'validation size: {drr_dataset.dataset_validation_size}')
        print('=' * 80 + '\n')

        # create model
        model = PositionPrediction(epochs=EPOCHS)
        model.build_model()
        model.compile_model()
        model.model.summary()

        history = model.fit_model(data_training=training_data,
                                  data_validation=validation_data,
                                  steps_per_epoch=steps_per_epoch,
                                  steps_per_validation=validation_steps)

        # TODO: save model
        print("\n[INFO] serializing network...".rjust(80, '='))
        model_name = '_'.join([BASE_MODEL, str(BATCH_SIZE),
                               str(EPOCHS), 'model']) + '.model'
        model.model.save(model_name)

        # TODO: eval model accuracy
        print(history)
        try:
            plot_metric(history)
        except NameError:
            print('Some NameError occurred when plotting! - skipped')
