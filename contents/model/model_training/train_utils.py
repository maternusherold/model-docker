#!/usr/env/bib python

import matplotlib.pyplot as plt
import numpy as np

from typing import List


# util function
def plot_metric(history, font_color: str = 'k', attr: List[str] = None) -> plt:
    """Plots training and validation accuracy. - for now only accuracy

    :param history: history returned from tf.keras model
    :param font_color: font color in plot
    :param attr: attribute to plot from the history
    """
    # mitigate mutable attribute complications
    if attr is None:
        attr = ['loss', 'val_loss']
    loss = history.history[attr[0]]
    val_loss = history.history[attr[1]]

    x = np.arange(1, len(loss) + 1, 1)

    plt.figure(figsize=(8, 8))
    plt.plot(x, loss, label='Training Loss')
    plt.plot(x, val_loss, label='Validation Loss')

    plt.legend(loc='upper right')
    plt.xlabel('Epochs', color=font_color)
    plt.ylabel('Loss', color=font_color)

    plt.xticks(x, color=font_color)
    plt.yticks(color=font_color)
    plt.title('Training and Validation mse', color=font_color)
    return plt
