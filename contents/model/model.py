#!/usr/env/bin python

import tensorflow as tf
import tensorflow.keras.applications as tf_models

from pipeline.dataset import DRRDataset


class PositionPrediction:

    def __init__(self, batch_size: int = 32, epochs: int = 10,
                 learning_rate: float = .001,
                 base_model: tf_models = tf_models.MobileNetV2(
                     input_shape=(224, 224, 3), include_top=False,
                     weights='imagenet'), base_model_trainable_layers: int = 0,
                 output_dimensions: int = 6):
        """TODO: docstring

        :param batch_size:
        :param epochs:
        :param learning_rate:
        :param base_model:
        :param base_model_trainable_layers:
        :param output_dimensions:
        """
        self.batch_size = batch_size
        self.epochs = epochs
        self.learning_rate = learning_rate
        self.base_model = base_model
        self.trainable_layers = base_model_trainable_layers
        self.output_dimensions = output_dimensions
        self.model = None

    def build_model(self, **kwargs):
        """TODO: docstring

        :param kwargs:
        :return:
        """
        # TODO: check for trainable layers - if any or just False
        # ...
        self.base_model.trainable = False

        # TODO: set trainable layers
        # ...

        model = tf.keras.Sequential([
            self.base_model,
            tf.keras.layers.GlobalAveragePooling2D(),
            # FIXME: need to change to output dim and no activation
            tf.keras.layers.Dense(self.output_dimensions)
        ])
        self.model = model
        return model

    def compile_model(self):
        """TODO: docstring """
        # FIXME: provide optimizer via constructor
        optimizer = tf.keras.optimizers.Adam(lr=self.learning_rate)
        self.model.compile(optimizer=optimizer, loss='mean_squared_error',
                           metrics=['accuracy'])

    def fit_model(self, data_training: tf.data.Dataset,
                  data_validation: tf.data.Dataset, steps_per_epoch: int = 1,
                  steps_per_validation: int = 1, verbose: bool = True):
        """TODO: docstring

        :return: training history
        """
        if verbose:
            print(f'\n[INFO] {"=" * 73}')
            print(f'training for {self.epochs} epochs')
            print(f'training with {steps_per_epoch} steps per epoch')
            print('=' * 80 + '\n')

        history = self.model.fit(x=data_training, epochs=self.epochs, verbose=1,
                                 validation_data=data_validation,
                                 steps_per_epoch=steps_per_epoch,
                                 validation_steps=steps_per_validation,
                                 workers=0, use_multiprocessing=False)
        return history
